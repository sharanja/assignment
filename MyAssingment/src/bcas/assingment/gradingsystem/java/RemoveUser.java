package bcas.assingment.gradingsystem.java;
import java.util.Scanner;

public class RemoveUser {
	
		static Scanner sc = new Scanner(System.in);

		public static String[] removeX(String[] arr) {
			int index = sc.nextInt();
			int index1 = index - 1;
			if (arr == null || index1 < 0 || index1 >= arr.length) {
				return arr;
			}
			String[] anotherArray = new String[arr.length - 1];
			for (int i = 0, k = 0; i < arr.length; i++) {
				if (i == index1) {
					continue;
				}
				anotherArray[k++] = arr[i];
			}
			return anotherArray;
		}
	}
	
