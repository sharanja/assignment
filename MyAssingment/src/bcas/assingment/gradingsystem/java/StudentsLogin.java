package bcas.assingment.gradingsystem.java;

import java.util.Scanner;
public class StudentsLogin {
	static String [] username = new String[] {"sharah","presti"};
	static String [] password =new String[] {"123","456"};
	static String userOne = "", userOneP = "";

	static int storeLog = 0;
	static int length = username.length;

	public static void Login() {
		
		try {
			Scanner scan = new Scanner(System.in);

		System.out.println("--------------------------- STUDENTS LOGIN --------------------------");
		System.out.print("Enter username:");
		userOne = scan.next();
		System.out.print("Enter password:");
		userOneP = scan.next();

		for (int i = 0; i <= length; i++) {
			if (userOne.equals(username[i])) {
				storeLog = i;
				break;
			}
		}
		Checker();
		}catch(Exception e) {
			System.out.println("Wrong UserName & Password-------!");
			System.out.println("");
			Login();
		}
	}

	static void Checker() {
		if (userOne.equals(username[storeLog]) && userOneP.equals(password[storeLog])) {
			System.out.println("");
			System.out.println("-------------------------- View Report Card ---------------------------");
			System.out.println("------------------------------ PRESS 1 --------------------------------");
			System.out.println("");
			System.out.println("------------------------------- Exit ----------------------------------");
			System.out.println("------------------------------ PRESS 2 --------------------------------");
			System.out.println("");

			Scanner scan2 = new Scanner(System.in);
			int option = scan2.nextInt();

			switch (option) {
			case 1:
				for (int i = 0; i <= length; i++) {
					if (userOne.equals(username[i])) {
						storeLog = i;
						break;
					}
				}
				System.out.println("");
				System.out.println("***********************************************************************");
				System.out.println("\t\t\t\tREPORT CARD");	
				System.out.println("Student Name : " + StudentsLogin.username[storeLog]);
				System.out.println(" _____________________________________________________________________");
				System.out.println("| \t Details \t"+"|  Student Marks  |"+"  Grade  |"+"  Highest Marks  ");
				System.out.println("|_______________________|_________________|_________|_________________|");
				System.out.print("|  Physics Marks \t|\t" + TeacherLogin.Phy[storeLog]+"\t  |   ");
				System.out.print(Grade.findgrade(TeacherLogin.Phy[storeLog])+"   |\t    ");
				FindHighestMarks.largest(TeacherLogin.Phy);
				System.out.println("|_______________________|_________________|_________|_________________|");
				System.out.print("|  Chemistry Marks \t|\t" + TeacherLogin.Che[storeLog]+"\t  |   ");
				System.out.print(Grade.findgrade(TeacherLogin.Che[storeLog])+"   |\t    ");
				FindHighestMarks.largest(TeacherLogin.Che);
				System.out.println("|_______________________|_________________|_________|_________________|");
				System.out.print("|  Maths Marks \t \t|\t" + TeacherLogin.Mat[storeLog]+"\t  |   ");
				System.out.print(Grade.findgrade(TeacherLogin.Mat[storeLog])+"   |\t    ");
				FindHighestMarks.largest(TeacherLogin.Mat);
				System.out.println("|_______________________|_________________|_________|_________________|");
				System.out.println("|  Total Marks \t \t|\t\t      "+ Sum.sum(TeacherLogin.Che[storeLog], TeacherLogin.Phy[storeLog], TeacherLogin.Mat[storeLog]) );								
				System.out.println("|_______________________|_____________________________________________|");
				System.out.println("");

				System.out.println("                             Thank You!!!");
				System.out.println("                                AND");
	
			case 2:
				System.out.println("                 System Going to ShutDown.........!!!!!");
				System.out.println("");
				Login();
				break;
			default:
				System.out.println("You Entered wrong Option");	
				Checker();
			}
		} else {
			System.out.println("Wrong UserName & Password!");
			System.out.println("");
			Login();
		}
	}
}