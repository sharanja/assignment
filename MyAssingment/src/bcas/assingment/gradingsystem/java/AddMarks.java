package bcas.assingment.gradingsystem.java;

import java.util.Scanner;

public class AddMarks {
	static Scanner scan = new Scanner(System.in);

	public static int[] addX(int arr[]) {
		
		int[] markArray = new int[arr.length+1];
		 int addMarks;
		 System.out.println("Enter the marks to add");
		 addMarks=scan.nextInt();
	 
	    for(int i = 0; i < arr.length; i++) {
	        markArray[i] = arr[i];
	    }
	 
	    markArray[markArray.length - 1] = addMarks;
	    return markArray;
	}	
}