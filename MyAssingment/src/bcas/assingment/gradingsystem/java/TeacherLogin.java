package bcas.assingment.gradingsystem.java;


import java.util.Arrays;
import java.util.Scanner;

public class TeacherLogin {

	 static String userOne = "", userOneP = "";
	 static Scanner scan;
	 static Scanner num;
	
	 static String username[] = { "Tom", "Jerry", "Mickey" };
	 static String password[] = { "123", "456", "789" };
	
	 static int Phy[] = {50,95};
	 static int Che[] = {60,42};
	 static int Mat[] = {70,99};
	
	 static int option;
	 static int storeLog = 0;
	 static int length = username.length;


	public static void Login() {
		
		try {
		scan = new Scanner(System.in);

			System.out.println("-------------------------- TEACHERS LOGIN ---------------------------");
			System.out.println("");
			System.out.print("Enter username :");
			userOne = scan.next();
			System.out.print("Enter password:");
			userOneP = scan.next();

		for (int i = 0; i <= length; i++) {
			if (userOne.equals(username[i])) {
				storeLog = i;
				break;
			}
		}
		Checker();
		}catch(Exception e) {
			System.out.println("Wrong username & password!");
			System.out.println("");
			System.out.println("If you want to go to Teacher Login again --> Press 1");
			System.out.println("If you want to go to Student Login  --> Press 2");
			int select = scan.nextInt();
			if(select == 1) {
				TeacherLogin.Login();
			}else if(select == 2) {
				StudentsLogin.Login();
			}else {
				System.out.println("System go to Shutdown");
			}
		
		}
	}

	static void Checker() {
		if (userOne.equals(username[storeLog]) && userOneP.equals(password[storeLog])) {
			System.out.println("Login Success");
			assaign();
		} else {
			System.out.println("Wrong username & password!");
			System.out.println("");
			Login();
		}
	
	}

	static void assaign() {
		num = new Scanner(System.in);	
		System.out.println("");
		System.out.println("Add Student >> press 1");
		System.out.println("Add Maths marks >> press 2");
		System.out.println("Add Chemistry marks >> press 3");
		System.out.println("Add Physics marks >> press 4");
		System.out.println("Find Chemistry Highest marks >> press 5");
		System.out.println("Find Physics Highest marks >> press 6");
		System.out.println("Find Maths Highest marks >> press 7");
		System.out.println("Print ReportCard >> press 8");
		System.out.println("Whole students grade with name >> press 9");
		System.out.println("Remove Students >> press 10");
		System.out.println("Remove Maths marks >> press 11");
		System.out.println("Remove Chemistry marks >> press 12");
		System.out.println("Remove Physics marks >> press 13");		
		System.out.println("Shutdown the system >> press 14");

		
		option = num.nextInt();

		switch (option) {
		case 1:
			System.out.println("Add New Student");
			System.out.println("Username" + Arrays.toString(StudentsLogin.username));
			StudentsLogin.username = AddUser.add(StudentsLogin.username);
			System.out.println("After Add Student Name" + Arrays.toString(StudentsLogin.username));
			System.out.println("");
			System.out.println("Passwords" + Arrays.toString(StudentsLogin.password));
			StudentsLogin.password = AddUser.add(StudentsLogin.password);
			System.out.println("After Add Password " + Arrays.toString(StudentsLogin.password));
			System.out.println("");
			
			System.out.println("Add Maths Marks ");
			TeacherLogin.Mat = AddMarks.addX(Mat);
			System.out.println("" + Arrays.toString(Mat));
			System.out.println("");

			System.out.println("Add Chemistry Marks ");
			TeacherLogin.Che = AddMarks.addX(Che);
			System.out.println("" + Arrays.toString(Che));
			System.out.println("");

			System.out.println("Add Physics Marks  ");
			TeacherLogin.Phy = AddMarks.addX(Phy);
			System.out.println("" + Arrays.toString(Phy));
		
			assaign();
			
			break;
			
		case 2:
			System.out.println("Add Maths Marks ");
			TeacherLogin.Mat = AddMarks.addX(Mat);
			System.out.println("" + Arrays.toString(Mat));
			System.out.println("");

			assaign();
			
			break;
			
		case 3:
			System.out.println("Add Chemistry Marks ");
			TeacherLogin.Che = AddMarks.addX(Che);
			System.out.println("" + Arrays.toString(Che));
			System.out.println("");

			assaign();
			
			break;
			
		case 4:
			
			System.out.println("Add Physics Marks  ");
			TeacherLogin.Phy = AddMarks.addX(Phy);
			System.out.println("" + Arrays.toString(Phy));

			assaign();
			
			break;

		case 5:
			System.out.println("Chemistry Highest Marks");
			FindHighestMarks.largest(Che);
			
			assaign();

			break;
		case 6:
			System.out.println("Physics Highest Marks");
			FindHighestMarks.largest(Phy);

			assaign();
			
			break;		
		case 7:
			System.out.println("Maths Highest Marks");
			FindHighestMarks.largest(Mat);
			
			assaign();
			
			break;
		case 8:

			for (int i1 = 0; i1 < StudentsLogin.username.length; i1++) {
				System.out.println("");
				System.out.println("***********************************************************************");
				System.out.println("\t\t\t\tREPORT CARD");	
				System.out.println("Student Name : " + StudentsLogin.username[i1]);
				System.out.println(" _____________________________________________________________________");
				System.out.println("| \t Details \t"+"|  Student Marks  |"+"  Grade  |"+"  Highest Marks  ");
				System.out.println("|_______________________|_________________|_________|_________________|");
				System.out.print("|  Physics Marks \t|\t" + Phy[i1]+"\t  |   ");
				System.out.print(Grade.findgrade(Phy[i1])+"   |\t    ");
				FindHighestMarks.largest(Phy);
				System.out.println("|_______________________|_________________|_________|_________________|");
				System.out.print("|  Chemistry Marks \t|\t" + Che[i1]+"\t  |   ");
				System.out.print(Grade.findgrade(Che[i1])+"   |\t    ");
				FindHighestMarks.largest(Che);
				System.out.println("|_______________________|_________________|_________|_________________|");
				System.out.print("|  Maths Marks \t \t|\t" + Mat[i1]+"\t  |   ");
				System.out.print(Grade.findgrade(Mat[i1])+"   |\t    ");
				FindHighestMarks.largest(Mat);
				System.out.println("|_______________________|_________________|_________|_________________|");
				System.out.println("|  Total Marks \t \t|\t\t      "+ Sum.sum(Che[i1], Phy[i1], Mat[i1]) );								
				System.out.println("|_______________________|_____________________________________________|");
				System.out.println("");
			}
			
			assaign();
			
			break;
		case 9:
				System.out.println("-------- whole students grade with name --------");
			for (int i1 = 0; i1 < StudentsLogin.username.length; i1++) {
				System.out.println("");
				System.out.println(" _______________________");	
				System.out.println("|Student Name : " + StudentsLogin.username[i1] +"\t|");
				System.out.println("|_______________________|");	
				System.out.print(Grade.findgrade(Phy[i1])+"   |\t    ");
				System.out.println("Physics");
				System.out.print(Grade.findgrade(Che[i1])+"   |\t    ");
				System.out.println("Chemistry");
				System.out.print(Grade.findgrade(Mat[i1])+"   |\t    ");
				System.out.println("Maths");
				System.out.println("");
			}
						
			assaign();			
			break;

		case 10:
			System.out.println("Remove Student");
			System.out.println("Username" + Arrays.toString(StudentsLogin.username));
			StudentsLogin.username = RemoveUser.removeX(StudentsLogin.username);
			System.out.println("After remove Student Name" + Arrays.toString(StudentsLogin.username));
			System.out.println("Passwords" + Arrays.toString(StudentsLogin.password));
			StudentsLogin.password = RemoveUser.removeX(StudentsLogin.password);
			System.out.println("After remove Password " + Arrays.toString(StudentsLogin.password));
			System.out.println("");	
			
			System.out.println("Remove Maths Marks ");
			System.out.println("Maths marks" + Arrays.toString(Mat));			
			TeacherLogin.Mat = RemoveMarks.removeM(Mat);
			System.out.println("" + Arrays.toString(Mat));
			System.out.println("");
			
			System.out.println("Remove Chemistry Marks ");
			System.out.println("Chemistry marks" + Arrays.toString(Che));			
			TeacherLogin.Che = RemoveMarks.removeM(Che);
			System.out.println("" + Arrays.toString(Che));
			System.out.println("");
			
			System.out.println("Remove Physics Marks ");
			System.out.println("Physics Marks" + Arrays.toString(Phy));			
			TeacherLogin.Phy = RemoveMarks.removeM(Phy);
			System.out.println("" + Arrays.toString(Phy));
			System.out.println("");
			
			assaign();
			break;
			
		case 11:			
			System.out.println("Remove Maths Marks ");
			System.out.println("Maths marks" + Arrays.toString(Mat));			
			TeacherLogin.Mat = RemoveMarks.removeM(Mat);
			System.out.println("" + Arrays.toString(Mat));
			System.out.println("");
			
			assaign();
			break;
			
		case 12:			
			System.out.println("Remove Chemistry Marks ");
			System.out.println("Chemistry marks" + Arrays.toString(Che));			
			TeacherLogin.Che = RemoveMarks.removeM(Che);
			System.out.println("" + Arrays.toString(Che));
			System.out.println("");

			assaign();
			break;
			
		case 13:			
			System.out.println("Remove Physics Marks ");
			System.out.println("Physics Marks" + Arrays.toString(Phy));			
			TeacherLogin.Phy = RemoveMarks.removeM(Phy);
			System.out.println("" + Arrays.toString(Phy));
			System.out.println("");

			assaign();
			break;
			
		case 14:
			System.err.println("The system Going To ShutDown........!!");
			StudentsLogin.Login();
			
			break;			
		default:
			System.out.println("You Entered wrong Option");	
			assaign();

		}

	}


}