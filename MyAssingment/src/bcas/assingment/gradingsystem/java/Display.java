package bcas.assingment.gradingsystem.java;

import java.util.Scanner;

import bcas.assingment.gradingsystem.java.StudentsLogin;
import bcas.assingment.gradingsystem.java.TeacherLogin;

public class Display {
		public static void main(String[] args) {
		int option;
		Scanner scan = new Scanner(System.in);

		System.out.println("****************************** WELCOME ******************************");
		System.out.println("");
		System.out.println("-------------------------- TEACHERS LOGIN ---------------------------");
		System.out.println("----------------------------- PRESS 1 -------------------------------");
		System.out.println("");
		System.out.println("*********************************************************************");
		System.out.println("");
		System.out.println("-------------------------- STUDENTS LOGIN ---------------------------");		
		System.out.println("----------------------------- PRESS 2 -------------------------------");
		
		option = scan.nextInt();
		switch (option) {
		case 1:
			TeacherLogin.Login();			
			break;
		case 2:
			StudentsLogin.Login();
		}
		}
}