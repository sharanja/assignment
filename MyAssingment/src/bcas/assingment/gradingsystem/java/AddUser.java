package bcas.assingment.gradingsystem.java;

import java.util.Scanner;

public class AddUser {
	static Scanner scan = new Scanner(System.in);

	public  static String[] add(String srcArray[]) {
		String[] addArray = new String[srcArray.length+1];
		 String elementToAdd;
		 System.out.println("Enter the element to add");
		 elementToAdd=scan.nextLine();
	 
	    for(int i = 0; i < srcArray.length; i++) {
	        addArray[i] = srcArray[i];
	    }
	 
	    addArray[addArray.length - 1] = elementToAdd;
	    return addArray;
	}
}